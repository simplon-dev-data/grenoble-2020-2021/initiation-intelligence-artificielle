# Initiation Intelligence Artificielle

- Dans un premier temps, nous allons travailler avec des modèles de machine learning déjà entrainés issus du cloud Azure. Il s'agit de l'activité contenue dans le notebook "IA_deja_entrainee.ipynb".

- Dans un second temps, nous entrainerons un modèle simple de classification. Il s'agit de l'activité contenue dans le notebook "IA_a_creer.ipynb".